KIND_CLUSTER_NAME := eck

up: kind_create_cluster set_context
down: kind_delete_cluster

# Common -----------------------------------------

set_context:
	@kubectl config use-context kind-$(KIND_CLUSTER_NAME)

# Kind cluster -----------------------------------

# Create Kind cluster
kind_create_cluster:
	@kind create cluster -n $(KIND_CLUSTER_NAME) --config ./kind/cluster.config.yaml

# Delete Kind cluster
kind_delete_cluster:
	@kind delete cluster -n $(KIND_CLUSTER_NAME)

# ECK --------------------------------------------

# Get Helm chart locally
eck_pull:
	@helm repo add elastic https://helm.elastic.co
	@helm repo update
	@helm pull elastic/eck-operator --version 2.10.0 -d charts --untar

eck_install_operator:
	@helm install elastic-operator charts/eck-operator -n elastic-system --create-namespace

eck_monitor_operator:
	@kubectl -n elastic-system logs -f statefulset.apps/elastic-operator

# ES ---------------------------------------------

es_install:
	@kubectl apply -f manifests/elasticsearch.yaml

es_pods:
	@kubectl get pods --selector='elasticsearch.k8s.elastic.co/cluster-name=quickstart'

es_logs:
	@kubectl logs -f quickstart-es-default-0

es_svc:
	@kubectl get service quickstart-es-http

es_proxy_forward:
	@kubectl port-forward service/quickstart-es-http 9200

es_test_localhost:
	@curl -u "elastic:$(shell kubectl get secret quickstart-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')" -k "https://localhost:9200"

# Kibana -----------------------------------------

kb_install:
	@kubectl apply -f manifests/kibana.yaml

kb_pods:
	@kubectl get pod --selector='kibana.k8s.elastic.co/name=quickstart'

kb_svc:
	@kubectl get service quickstart-kb-http

kb_proxy_forward:
	@kubectl port-forward service/quickstart-kb-http 5601

kb_secret:
	@kubectl get secret quickstart-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo
